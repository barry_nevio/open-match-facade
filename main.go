package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	// set server mode
	gin.SetMode(gin.DebugMode)

	// Instanciate default gin router
	router := gin.Default()

	// Test Get
	router.GET("/om-facade", func(c *gin.Context) {
		output := make(map[string]interface{})

		output["kek"] = "kekeroni - GET"

		c.JSON(200, output)
		return
	})

	// Test Post
	router.POST("/om-facade", func(c *gin.Context) {
		output := make(map[string]interface{})

		output["kek"] = "kekeroni - POST"

		c.JSON(200, output)
		return
	})

	addr := ":8990"
	router.Run(addr)

}
