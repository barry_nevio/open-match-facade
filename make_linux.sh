#!/bin/sh

env GOOS=linux GOARCH=amd64 go build -o main main.go
rm main.zip
zip main.zip main